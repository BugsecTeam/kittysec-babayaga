#import threading #not implemented yet, no reason to import it
import argparse,socket
from dns import reversename
import dns.name, dns.query, dns.zone, dns.resolver
import colorama
colorama.init()

##############################
# TBD:
# Progress bar for brute-force
# Find AS
# check port 53 and try a zone transfer
# Use Robtex
# do a traceroute to each ip, build a network architecture map
# Extract emails
# Extract documents
# take snapshots of port 80,443
##############################
# Dependencies:
# colorama,dnspython
# easily installed using pip
# go to python27\scripts
# commandline
# pip install colorama
# pip install dnspython
##############################

#Colors
white = '\e[1;37m' # pretty damn sure this is broken
dgray = '\x1b[90m'
DGRAY = '\x1b[100m'
lred = '\x1b[91m'
LRED = '\x1b[101m'
lgreen = '\x1b[92m'
LGREEN = '\x1b[102m'
lyellow = '\x1b[93m'
LYELLOW = '\x1b[103m'
lblue = '\x1b[94m'
LBLUE = '\x1b[104m'
lmagneta = '\x1b[95m'
LMAGENTA = '\x1b[105m'
lcyan = '\x1b[96m'
LCYAN = '\x1b[106m'
lgray = '\x1b[97m'
LGRAY = '\x1b[107m'

#Create a list that holds ALL unique sub-domains
glob_domain_list = list()
glob_ip_list = list()
glob_domain = ''
glob_subnets = list()

helpmsg=lgreen+'''
AUTHOR IS NOT RESPONSIBLE TO ANY DAMAGE CAUSED BY THIS TOOL
PLEASE USE WITH CAUTION AND ONLY IF YOU HAVE AUTHORIZATION

                                 Baba-Yaga
                                 Sub Domain Extractor
               (       "     )   and Passive Information
                ( _  *           Gathering Tool V1.0 by KittySec(C)
                   * (     /      \    ___
                      "     "        _/ /
                     (   *  )    ___/   |
                       )   "     _ o)'-./__
                      *  _ )    (_, . $$$
                      (  )   __ __ 7_ $$$$
                       ( :  { _)  '---  $\\
                   ______'___//__\   ____,\\
                   )           ( \_/ _____\_
                 .'             \   \------"".
                 |="           "=|  |         )
                 |               |  |  .    _/
                  \    (. ) ,   /  /__I_____/
                   '._/_)_(\__.'   (__,(__,_]
                  @---()_.'---@"" "" `""
                '''

parser = argparse.ArgumentParser(prog="babayaga.py",
                                 formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-d',dest="DOMAIN",type=str,help="Domain to babayaga, for example: kittysec.com",required=True)
parser.add_argument('-v',action="store_true",dest="VERBOSE",default=False,help="Verbose.")
parser.add_argument('-o',dest="FILENAME",type=str,default='domains.txt',help="Domains wordlist. Default: domains.txt")
options=vars(parser.parse_args())
READ_ONLY='r'
domain = options['DOMAIN'] #example.com
VERBOSE = options['VERBOSE']
FILENAME = options['FILENAME']
glob_domain = domain

print(helpmsg)

#########################
#NETCRAFT_CODE Goes Here#
#########################

def bruteForce():
    #This method uses a Brute-Force technique in order to find subdomains and map them to their ip.
    print (lgray+'\n[*] Now using a Brute-Force in order to find more sub-domains.\n[*] This may take a while...')
    resolver = dns.resolver.Resolver()
    domainSuffix = '.'+glob_domain
    f = open(FILENAME, READ_ONLY)
    fileData=f.read().split('\n')
    for subDomain in fileData:
        subDomain = subDomain.strip()+domainSuffix
        try:
            result = resolver.query(subDomain, 'A')
            for ip in result:
                print ('%s FOUND using Brute-Force technique: %s\t%s'%(lgreen+'[+]',subDomain,ip))
                checkUnique(subDomain)
                addtoglobiplist(domain2ip(subDomain))
        except dns.resolver.NXDOMAIN:
            # Documentation:
            # @raises NXDOMAIN: the query name does not exist
            if VERBOSE:
                 print('%s Does not exist: %s'%(lred+'[-]',subDomain))
            continue
        except:
            print ("%s Something unexpected happened. Last bruteforce attempt was: %s"%(lred+'[-]',subDomain))

#Makes a list of unique IP lists
def addtoglobiplist(ip):
    if ip not in glob_ip_list:
        glob_ip_list.append(ip)

#Converts a domain name to IP
def domain2ip(domain):
    try:
        return socket.gethostbyname(domain)
    except:
        pass

#Converts an IP to a hostname
def ip2domain(subnet, orgName):
    try:
        reversedDNSPossibleHits=[]
        r = dns.resolver.Resolver()
        for x in range(1,255):
            newSubnet = subnet + str(x)
            rev_name = reversename.from_address(newSubnet)
            try:
                reversedDns = str(r.query(rev_name,"PTR")[0])[:-1]
            except:
                continue
            if orgName in reversedDns:
                print ('%s[+] FOUND! Using reverse DNS technique: %s'%(lgreen,reversedDns))
                checkUnique(reversedDns)
            else:
                reversedDNSPossibleHits.append(reversedDns)
        return reversedDNSPossibleHits
    except:
        print (lred + '[-] Something went wrong while trying to convert IP to hostname')

#reverse DNS lookup
def reverseDNS(subnet, orgName,counter):
    counter = str(counter)
    print ('%s[!] Now scanning subnet %s out of %s using reverse DNS lookup.\n[!] This may take a while...'%(lyellow,lmagneta+counter,str(len(glob_subnets))+lyellow))
    subnet+='.'
    ip2domain(subnet, orgName)

#make a list of unique hostnames
def checkUnique(domain):
    if domain not in glob_domain_list:
        glob_domain_list.append(domain)

#make a list of unique subnets
def checkUniqueSubnets(ip):
    # TODO: Get nearest multiple for a subnet
    if ip not in glob_subnets:
        glob_subnets.append(ip)

def axfr(nameserver):
    # TODO: Check this method
    global glob_domain
    try:
        print (lgray + '[!] Attempting a Zone Transfer on ' + lred + nameserver)
        zone = dns.zone.from_xfr(dns.query.xfr(nameserver, glob_domain))
        names = zone.nodes.keys()
        if names:
            print (lgreen + '[+] Zone Transfer Succeeded!!!')
            for n in names:
                n = str(n) + '.' + glob_domain
                checkUnique(n)
    except:
        print (lgray + '[-] Zone Transfer failed')

def getauth(domain,orgName):
    try:
        boolexternal = False
        boolinternal = False
        ownsubdomains = []
        externaldomains = []
        xferTargets=[]
        print ('%s[*] Trying to extract Authorative nameservers for %s'%(lgray,lred+domain+lgray))
        for nameServer in dns.resolver.query(domain, 'NS'):
        #Check if the target manages their DNS by their own...
            nameServer = str(nameServer)[:-1]
            if orgName in nameServer:
                boolinternal = True
                ownsubdomains.append(nameServer)
                checkUnique(nameServer)
            else:
                boolexternal = True
                externaldomains.append(nameServer)

            if boolinternal:
                if boolexternal:
                    print '[!] It seems that the target has DNS servers both internally and externally.'
                for subDom in ownsubdomains:
                    print ("%s FOUND! %s %s"%(lgreen+'[+]',subDom,domain2ip(subDom)))
                    checkUnique(subDom)
            for extDom in externaldomains:
                # Prevent multiple messages containing the same information
                if extDom not in xferTargets:
                    print ('%s External NS Found %s %s'%(lmagneta+'[-]'+lgray,extDom,domain2ip(extDom)))
            ##########################
            #DEAD_CODE_AUTH GOES HERE#
            ##########################
            # Zone transfer targets
            xferTargets+=externaldomains+ownsubdomains
        newXfer=[]
        for transferables in xferTargets:
            # Prevent attempts to zone transfer the same name server
            if transferables not in newXfer:
                newXfer.append(transferables)

        for dom in newXfer:
            # Zone transfer
            axfr(dom)
    except:
        print ("Couldn't extract Authorative Nameservers for " + domain)

#Find MX Servers
def getMX(domain,orgName):
    try:
        bool = False
        ownsubdomains = []
        externaldomains = []
        print ('%s[*] Trying to extract MX servers for %s' %(lgray,lred+domain+lgray))
        for incomingRelay in dns.resolver.query(domain,'MX'):
            relayName=str(incomingRelay.exchange)
            if orgName in relayName:
                bool = True
                ownsubdomains.append(relayName)
                checkUnique(relayName)
            else:
                externaldomains.append(x)
            #print all MX sub-domains found
        if bool:
            for _domain in ownsubdomains:
                _domain = _domain[:-1]
                print ('%s FOUND! %s %s'%(lgreen+'[+]',str(_domain),domain2ip(_domain)))
        #if mail services are being managed externally...
        else:
            print ('%s It appears that the target %s manages their mail services in an external source... print anyways just to make sure?(Y/N)'%(lyellow+'[?]'+lgray,lred+domain+lgray))
            userAnswer = input().lower()
            if userAnswer == 'y':
                for i in externaldomains:
                    print ('%s %s'%(lmagenta+'[-] '+lgray+i,domain2ip(str(i)[:-1])))
    except:
        print (lred+"[-] Couldn't convert domain name to IP "+domain)

def uniqueList(l):
    # Filter duplicates in a list
    newL=[]
    for item in l:
        if item not in newL:
            newL.append(item)
    return newL

def main():
    global glob_domain
    orgName = domain.split('.')[0]
    print ('%s[*] Trying to convert domain name to IP...'%lgray)
    print ('%s Successfully Converted %s to IP %s'%(lgreen+'[+]'+lgray,lred+domain+lgray,domain2ip(domain)))
    domainSubnet = ".".join(domain2ip(domain).split('.')[:3])
    checkUniqueSubnets(domainSubnet)
    getMX(domain,orgName)
    getauth(domain,orgName)
    ########################################
    #NOT_SURE_WHAT_YOU_MEANT_CODE goes here#
    ########################################
    bruteForce()
    # END of Brute-Force
    # robtex(domain,orgName)
    for d in glob_domain_list:
        # Build a list of all unique IPs found
        addtoglobiplist(domain2ip(d))

    for ip in glob_ip_list:
        # Build a list of unique subnets
        subnet = ".".join(ip.split('.')[:3])
        checkUniqueSubnets(subnet)

    #reverse DNS Lookup
    print ('%s[+] Found the following subnets:'%lgreen)
    print (lgreen+'='*32)
    for subnet in glob_subnets:
        print ('[+] %s.0/24'%subnet)

    counter=1
    # Possible Reverse DNS hits that may need manual review
    possibleHits=[]
    for subnet in glob_subnets:
        # TODO: use reverseDNS technique on NEWLY discovered IP address subnets that resulted from this Reverse DNS
        possibleHits.append(reverseDNS(subnet, orgName, counter))
        counter+=1

    #Print a list of all the results
    print (lgray+'\n[+] Compiling a list of all sub-domains harvested...\n%s'%('='*50))
    # Print IPS found.
    domList=[]
    for d in glob_domain_list:
        domList.append(domain2ip(d))
    print "\n".join(uniqueList(domList))
    print ("[+] This list may require manual review for extra possbile targets:")
    print("\n".join(possibleHits))

if __name__ == '__main__':
        main()



DEAD_CODE_AUTH="""if boolinternal and boolexternal:
                print ('[!] It seems that the target has DNS servers both internally and externally.')
                for subDom in ownsubdomains:
                    print ("%s FOUND! %s %s"%(lgreen+'[+]',subDom,domain2ip(subDom)))
                    checkUnique(subDom)

                for extDom in externaldomains:
                    print ('%s External NS Found %s %s'%(lmagneta+'[-]'+lgray,extDom,domain2ip(extDom)))

            elif (boolinternal and not boolexternal):
                for subDom in ownsubdomains:
                    print ("%s FOUND! %s %s"%(lgreen+'[+]',subDom,domain2ip(subDom)))
                    checkUnique(subDom)

            elif (not boolinternal and boolexternal):
                print ('%s It appears that the target %s manages their DNS services in an external source.\n%s Print anyway just to make sure?(Y/N)'%(lyellow+'[?]'+lgray,lred+domain+lgray,lyellow+'[?]'+lgray))
                userAnswer = raw_input()
                if userAnswer == 'y':
                    for subDom in externaldomains:
                        print ('%s External DNS Found %s %s'%(lmagneta+'[-]'+lgray,subDom,domain2ip(subDom)))"""

NOT_SURE_WHAT_YOU_MEANT_CODE="""
        resolver = dns.resolver.Resolver()
        testWildcard = resolver.query('djgkshdgjksdhg.' + domain, 'A')
        if testWildcard:
               print ('Wildcard DNS is in use... skipping')
        else:"""
        
NETCRAFT_CODE="""
#def netcraft(domain,orgName):
#try:
#       print ('[!] Trying to establish a connection with NetCraft...')
#       conn = http.client.HTTPConnection('searchdns.netcraft.com')
#       conn.request('GET', '/')
#       response = conn.getresponse()
#       data1 = response.read()
#       print (data1)
#xcept:
#       print (lred + '[-] Something went wrong while trying to connect to NetCraft')
"""